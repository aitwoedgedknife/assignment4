Q1. In test cases where Pacman is boxed in (which is to say, he is unable to change his
observation point), why does Pacman sometimes have trouble finding the exact location of the ghost?

    In second test case, 2-ExactObserve.test The Pacman is predicting ghost to be in all 4 corners. The
    pacman is not able to move and it is only getting evidence i.e evidence for ghost to be present
    at a location with some probability. After obtaining adequate evidence Pacman is now sure that the
    ghost is in one of the 4 corners but since pacman is unable to move, it has trouble deciding on the
    ghost location.However in the next test case 3-ExactObserve.test where pacman is able to move freely,
    it quickly confirms which of the corner positions the ghost is currently on based on its continuously
    updated belief.

Q2. For which of the test cases do you notice differences emerging in the shading of the
squares? Can you explain why some squares get lighter and some squares get darker?

    For the Test cases 2-ExactElapse.test and 3-ExactElapse.test we observe difference in shading of
    the squares. As time steps increase, pacman observes evidence which in turn updates its current
    belief.(In case if evidence is not observed periodically,the entire map would eventually have a
    uniform distribution of probabilities). In the above cases as pacman observes the evidence at a given
    timestep i.e the locations where the ghost is probably on for the given timestep, pacmans belief is
    updated. Over multiple time steps and subsequent belief calculations, the boxes where the
    ghost is less likely to occupy will have a darker shade due to its low probability values and the
    boxes with frequent or most probable occurences of the ghost will have a lighter shade due to
    higher probability values.

Q3. Notice the difference between test 1 and test 3. In both tests, pacman knows that the ghosts
will move to the sides of the gameboard. What is different between the tests, and why?

    In the first case, we notice that distribution of color shades is predominately similar in majority
    of the boxes.This is due to the fact that only elapseTime function is being called and no reference
    to Observe is made which handles the noisy distance(evidence) which in turn does belief calculations.
    However in Test 3, we can notice a distinct color separation between the 2 ghosts since we now have
    the evidence calculations using the noisy distances along with elapseTime as well. Since pacman can
    now make a better probabilistic calculation, he is more certain of which ghost is moving in which
    direction and hence the distinct color separation. 
